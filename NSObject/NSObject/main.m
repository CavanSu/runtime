//
//  main.m
//  NSObject
//
//  Created by CavanSu on 2019/7/19.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        id obj = [[NSObject alloc] init];
        
        NSLog(@"Hello, World!");
    }
    return 0;
}
