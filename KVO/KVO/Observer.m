//
//  Observer.m
//  KVO
//
//  Created by CavanSu on 2019/5/27.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import "Observer.h"
#import "Shop.h"

@implementation Observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"keyPath: %@", keyPath);
    NSLog(@"change dic: %@", change);
    
    if ([keyPath isEqualToString:@"price"]) {
        Shop *shop = (Shop *)object;
        NSLog(@"shop price: %f", shop.price);
        
        NSString *nContext = (__bridge NSString *)context;
        NSLog(@"context: %@", nContext);
    }
    
    if ([keyPath isEqualToString:@"information"]) {
        ShopWrapper *shop = (ShopWrapper *)object;
        NSLog(@"ShopWrapper information: %@", shop.information);
        
        NSString *nContext = (__bridge NSString *)context;
        NSLog(@"context: %@", nContext);
    }
}
@end
