//
//  main.m
//  KVO
//
//  Created by CavanSu on 2019/5/27.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "Shop.h"
#import "Observer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Shop *s = [[Shop alloc] init];
        s.price = 10;
        NSString *context = @"shop price changed";
        
        // 获取 isa 指针指向
        NSLog(@"object_getClass: %@", object_getClass(s));
        
        Observer *ob = [[Observer alloc] init];
        
#if obPirce
        [s addObserver:ob forKeyPath:@"price"
               options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
               context:(__bridge void * _Nullable)(context)];
#else
        // 观察依赖键
        ShopWrapper *wrapper = [[ShopWrapper alloc] initWithShop:s];
        
        [wrapper addObserver:ob forKeyPath:@"information"
                     options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                     context:nil];
#endif
        
        s.price = 30;
        
        // 获取 isa 指针指向
        NSLog(@"object_getClass: %@", object_getClass(s));
    }
    return 0;
}

