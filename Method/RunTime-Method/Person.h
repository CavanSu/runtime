//
//  Person.h
//  runMethod
//
//  Created by CavanSu on 17/5/9.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *sex;
- (void)study;
- (void)run;
@end
