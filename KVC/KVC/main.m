//
//  main.m
//  KVC
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KTest.h"

void test1() {
    KTest *t = [[KTest alloc] init];
    [t setValue:@"cavan" forKey:@"name"];
    
    @try {
        // 当 test 重写 accessInstanceVariablesDirectly 为 NO 时，KVC 调用 setValue: forUndefinedKey:
        // 会抛出异常
        [t setValue:@"test" forKey:@"title"];
    } @catch (NSException *exception) {
        NSLog(@"exception: %@", exception.reason);
    } @finally {
        NSLog(@"finally");
    }
    
    NSLog(@"title: %@", [t valueForKey:@"title"]);
}

void test2() {
    NSArray* arrStr = @[@"english",@"franch",@"chinese"];
    NSArray* arrCapStr = [arrStr valueForKey:@"capitalizedString"];
    for (NSString* str  in arrCapStr) {
        NSLog(@"%@",str);
    }
    NSArray* arrCapStrLength = [arrStr valueForKeyPath:@"capitalizedString.length"];
    for (NSNumber* length  in arrCapStrLength) {
        NSLog(@"%ld",(long)length.integerValue);
    }
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        test2();
    }
    return 0;
}
