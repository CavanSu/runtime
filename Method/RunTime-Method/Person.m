//
//  Person.m
//  runMethod
//
//  Created by CavanSu on 17/5/9.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "Person.h"

@implementation Person
- (void)study {
    NSLog(@"study");
}

- (void)run {
    NSLog(@"run");
}
@end
