//
//  ViewController.m
//  Hook
//
//  Created by CavanSu on 2019/7/19.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import "ViewController.h"
#import "fishhook.h"

/*
 基地址每次启动都不一样，
 文件中的偏移地址固定
*/

/*
 静态语音编译时刻就必须知道所有的函数实现，就是知道函数的实现地址。
 
 但是在程序运行前，是无法得知函数地址的。
 
 PIC: 位置代码独立。 编译时, 函数在可执行文件中，被定义一个为一个 “符号”（8字节指针）
 
 DYLD: 加载所有动态库与 app。 在加载或者说运行时，绑定 -> 将 “符号”（8字节指针）指向实际的地址
*/

/*
 fishhook 只能 hook 系统函数;
 
 不能 hook 自定函数, 没有符号????
*/

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    struct rebinding nslog;
    nslog.name = "NSLog";
    nslog.replacement = myNSLog;
    nslog.replaced = (void *)&sys_nslog;
        
    struct rebinding rebs[1] = {nslog};
    
    // 重新绑定符号, 交换
    // 使 NSLog 的符号，指向 myNSLog 的实现
    // 使 myNSLog 的符号，指向 NSLog 的实现
    rebind_symbols(rebs, 1);
}

// 定义一个函数指针，用于保存原始的函数地址
static void(*sys_nslog)(NSString * _Nonnull format, ...);

void myNSLog(NSString * _Nonnull format, ...) {
    format = [format stringByAppendingString:@", hooked!!"];
    
    // 使用 NSLog 打印
    sys_nslog(format);
    
    NSLog(@"123");
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"test");
}

@end

// 一个 16进制位 等于 4wei

// image list: 打印加载模块列表
// 获取到 app 首地址 0x00000001009de000

// 从 macho 文件的 Section64(_DATA, _la_symbol_ptr), Lazy symbol pointers
// 获取到偏移值 offset 3028

// memory read 读取 首地址 + offset, 得到 NSLog 的地址,
// 一个指针占8个字节

// iOS cpu 是小端模式，反汇编读值时，从右往左读
// dis -s 取值这个指针的值
