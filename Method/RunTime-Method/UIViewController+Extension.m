//
//  UIViewController+Extension.m
//  runMethod
//
//  Created by CavanSu on 17/5/9.
//  Copyright © 2017年 CavanSu. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@implementation UIViewController (Extension)
// 加载进内存时调用
+ (void)load {
    Method method1 = class_getInstanceMethod(self, NSSelectorFromString(@"dealloc"));
    Method method2 = class_getInstanceMethod(self, @selector(cusdealloc));
    method_exchangeImplementations(method1, method2);
    
    // 交换的实际操作是，更改两个函数指针所指向的 函数体位置
    // 所以这里是
    // dealloc -> （cusdealloc implement）
    // cusdealloc -> (dealloc implement)
}

- (void)cusdealloc {
    NSLog(@"UIViewController cusdealloc");
    
    // 通过调用 [self cusdealloc];
    // 可以执行到 (dealloc implement) 的函数体
    [self cusdealloc];
}

- (void)dealloc {
    NSLog(@"UIViewController dealloc");
}
@end
