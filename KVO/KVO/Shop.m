//
//  Shop.m
//  KVO
//
//  Created by CavanSu on 2019/5/27.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import "Shop.h"

@implementation Shop
//- (void)setPrice:(float)price {
//    _price = price;
//}

// 当值发生改变时，会自动调用以下两个方法
// 不在 setter 中调用，重写 setPrice 依然会自动触发这两个方法
//- (void)willChangeValueForKey:(NSString *)key {
//    NSLog(@"key: %@", key);
//}
//- (void)didChangeValueForKey:(NSString *)key {
//    NSLog(@"key: %@", key);
//}

// 重写 automaticallyNotifiesObserversForKey return No. 可以使该类不能被观察
//+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
//    if ([key isEqualToString:@"price"]) {
//        return NO;
//    }
//    return [super automaticallyNotifiesObserversForKey:key];
//}
@end

@implementation ShopWrapper

- (instancetype)initWithShop:(Shop *)shop {
    self = [super init];
    if (self) {
        _shop = shop;
    }
    return self;
}

- (void)setInformation:(NSString *)information {
    [_shop setPrice:(information.floatValue)];
}

- (NSString *)information {
    return [[NSString alloc] initWithFormat:@"%f", _shop.price];
}

#if AffectingInformation
+ (NSSet<NSString *> *)keyPathsForValuesAffectingInformation {
    NSSet *keyPaths = [NSSet setWithObjects:@"shop.price", nil];
    return keyPaths;
}
#else
+ (NSSet<NSString *> *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    NSArray * moreKeyPaths = nil;
    
    if ([key isEqualToString:@"information"]) {
        moreKeyPaths = [NSArray arrayWithObjects:@"shop.price", nil];
    }
    
    if (moreKeyPaths) {
        keyPaths = [keyPaths setByAddingObjectsFromArray:moreKeyPaths];
    }
    
    return keyPaths;
}
#endif
@end
