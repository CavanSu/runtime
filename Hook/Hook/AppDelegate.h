//
//  AppDelegate.h
//  Hook
//
//  Created by CavanSu on 2019/7/19.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

