//
//  Shop.h
//  KVO
//
//  Created by CavanSu on 2019/5/27.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shop : NSObject
@property (nonatomic, assign) float price;
@end

@interface ShopWrapper : Shop
@property (nonatomic, copy) NSString *information;
@property (nonatomic, strong) Shop *shop;
- (instancetype)initWithShop:(Shop *)shop;
@end
