//
//  ViewController.m
//  runMethod
//
//  Created by CavanSu on 17/5/9.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <objc/runtime.h>
#import "ViewController.h"
#import "Person.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self propertys];
    [self methodsList];
    //    [self exchangeMethods];
}

// 调换两个方法的执行顺序
- (void)exchangeMethods {
    // 获取类中的一个对象方法
    Method method1 = class_getInstanceMethod([Person class], @selector(run));
    Method method2 = class_getInstanceMethod([Person class], @selector(study));
    
    // 获取类中的一个类方法
    // class_getClassMethod
    
    // 交换两个方法的函数体
    method_exchangeImplementations(method1, method2);
    
    Person *p = [[Person alloc] init];
    
    // 交换的实际操作是，更改两个函数指针所指向的 函数体位置
    // study -> （run implement）
    // run -> (study implement)
    [p study];
    [p run];
}

- (void)property {
    unsigned int count;
    
    // 获取类的属性列表
    Ivar *ivarArray = class_copyIvarList([Person class], &count);
    
    for (int i = 0; i < count; i++) {
        // 取出属性
        Ivar var = ivarArray[i];
        
        NSString *proName = @(ivar_getName(var));
        NSString *proType = @(ivar_getTypeEncoding(var));
        // ivar_getOffset(var); 获取属性内存偏移值
        
        NSLog(@"属性名 :%@", proName);
        NSLog(@"属性类型 :%@", proType);
    }
}

- (void)propertys {
    unsigned int count;
    
    objc_property_t *list = class_copyPropertyList([Person self], &count);
    
    for (int i = 0; i < count; i++) {
        objc_property_t item = list[i];
        const char *name = property_getName(item);
        const char *attributes = property_getAttributes(item);
        
        NSLog(@"name: %s, attributes: %s", name, attributes);
    }
    
    free(list);
}

- (void)methodsList {
    unsigned int count;
    
    Method *list = class_copyMethodList([Person class], &count);
    
    char dst;
    
    for (int i = 0; i < count; i++) {
        Method method = list[i];
        
        // Description
        struct objc_method_description *description = method_getDescription(method);
        
        SEL name = description->name;
        const char* cName = sel_getName(name);
        const char* cType = description->types;
        NSString *type = @(description->types);
        NSLog(@"method description: %s, type: %s, %@", cName, cType, type);
        
        // method_get
        SEL name_m = method_getName(method);
        const char* cName_m = sel_getName(name_m);
        
        int argumentsCount = method_getNumberOfArguments(method);
        const char* encoding = method_getTypeEncoding(method);
        method_getReturnType(method, &dst, sizeof(char));
        IMP imp = method_getImplementation(method);
        
        NSLog(@"method name: %s, argumentsCount: %d, encoding: %s", cName_m, argumentsCount, encoding);
        NSLog(@"method return value type: %c", dst);
    }
    
    free(list);
}



// method_getReturnType 返回类型 v, @
// v -> void
// @ -> id

@end
