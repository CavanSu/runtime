//
//  ViewController.swift
//  KVO_Thread
//
//  Created by CavanSu on 2019/6/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var shop: Shop?
    var observer: Observer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shop = Shop()
        shop?.price = 20
        
        observer = Observer()
        
        shop?.addObserver(observer!, forKeyPath: "price", options: [.old, .new], context: nil)
        NSLog("addObserver")
        
        DispatchQueue.global().async { [weak shop] in
            NSLog("isMainThread: %d", Thread.isMainThread)
            shop?.price = 50
        }
    }
}

